![Hotel Trader LLC](http://www.hoteltrader.com/images/logo-50-hotel-trader.png)
# HT PULL API

This document outlines the various endpoints that our API has exposed to allow our partners to:
- map the Hotel Trader property portfolio,
- implement the booking workflow,
- locate all of the requirements to commence and complete integration and certification.

# Credentials
Hotel trader uses base-64 encoded username/password based authorization over HTTPS to provide access to the API. Hotel Trader will provide the encoded token in advance of the commencement of integration for each environment.

# Payment Methods:

Hotel Trader supports both Direct-Bill (“DB”) and Pay-at-Booking (“PAB”) payment methods. 
A DB clients are typically pre-approved for a certain credit limit. Reservations are billed on the earlier of a) 3 days of Booking or b) Check-in date, unless otherwise agreed upon. If the credit limit is exceeded between payment dates, reservations will be rejected till a payment is made to the account to clear the past due or additional deposit is provided. DB clients are not required to send across the payment/credit card details in the `confirmReservation` method.

A PAB client is required to include the `paymentInformation` node in the `confirmReservation` request with the credit card information that needs to be charged at the time of making a booking. Please note that we highly recommend using Virtual Credit Cards (“VCC”) instead of a Corporate Credit Card as certain Financial Institutions limit repeated transactions from the same vendor as they use 3rd party fraud detection services.

# Authentication Headers: 


The Hotel Trader API is designed to work with `Content-Type=application/json`, so the client needs to send the following headers with each GET or POST request.

| ATTRIBUTE NAME | DESCRIPTION                                                                                                |
| -------------- | ---------------------------------------------------------------------------------------------------------- |
| Content-Type   | application/json                                                                                           |
| Authorization  | This is the authorization token provided by Hotel Trader that needs to be included in each request header. |
 
**NOTE: The token is unique to each client and must be kept secure.**



# Unique Attribute Persistence Requirement:
In the response for the Booking step `getPropertyByID`/`getPropertyByCity`/`getPropertyByRegionID`, a unique identifier - `htIdentifier` will be included. This unique key encapsulates the search attributes, pricing and other relevant details.

All subsequent requests in the Booking workflow (`getProperty`/`confirmPrice`/`confirmReservation`) require this `htIdentifier` attribute to be included in the requests payloads.


# Booking Workflow
The API is designed to support various UX/UI flows for our API clients. The booking flow consists of 3 steps
1. Search
2. Quote
3. Book


# 1. Search
#### Search by ID 
```getPropertyById```

Hoteltrader's search supports searching by `propertyID`. The list of `propertyID`'s can be found in your Client Portal under Mapping. Aside from that, clients can also connect and get a list of properties from the `/hotels` endpoint to get a full list of properties that are in the Hoteltrader portfolio.

#### Search by City/Region
```getPropertyByCity / getPropertyByRegionId```

Client's can also search the Hotel Trader portfolio using a city name or region ID. The list of region ID's can be located in your Client Portal under Mapping. Aside from that, clients can also get the list of Region ID's by calling the `/getRegions` endpoint. Hotel Trader can also provide other options for searches upon request that can include but are not limited to - "by Star Rating", "by Amenities", "by Point of Interest", "by Airport Code" etc. 

# 2. Quote
This is an optional step that is offered so client's can confirm that the price provided in the Search step is still valid. The response will provide the current price if there is a price change.

# 3. Book
This is the final step to confirm a reservation, and also encapsulates the logic to check that the price provided in the Search step is unchanged. In the event the price changes, the client will be given a response with the error code `1090 - Price has changed, please refresh search`. The new price is included in the response, and clients can still confirm the booking by sending in the new price by calling confirmReservation again.
We would like to note that the caching system that powers the Hotel Trader PULL API, is designed for < 1% cases where price changes occur during the Booking step due to caching issues.


# Property Mapping and Updates
Hotel Trader's property portfolio is constantly being updated and in being true to our Vision of a frictionless distribution environment, we have built a solution that will allow for clients to automatically map new properties as they are added. 
As a pre-requisite to certification, Clients will be required to implement an endpoint which Hoteltrader will periodically invoke to check on the status of the mapped properties in the client system. 
Using this functionality, we will then be able to provide you a status on what percentage of our portfolio is active on your shelf. Furthermore we will also be able to alert you as and when properties are added instead of sending periodic emails that are typically not actionable as they do not highlight the changes.
The details of the endpoint that needs to be implemented will be provided separately once integration commences.